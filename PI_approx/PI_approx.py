import sys

from math import sqrt
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button
from random import random


#Considérons un carré de côté 1 : son aire est donc de 12 = 1. 
#Considérons maintenant le quart de cercle de rayon 1 inscrit dans ce carré : son aire est 1/4 de l’aire du cercle complet de rayon 1, et donc l’aire du quart de cercle est π/4.

fig, ax = plt.subplots()
plt.subplots_adjust(left=0.25, bottom=0.25)

def distance(a,b):
    return sqrt((b[0]-a[0])*(b[0]-a[0]) + (b[1]-a[1])*(b[1]-a[1]))

def isInCircle(point, radius):
    return distance(point, [0,0]) <= radius

def computePoints(nbOfPoint):
    pointInCircle = 0
    totalOfPoint = 0
    # on tire aléatoirement le nombre de point entrés par l'utilisateur
    for i in range(int(nbOfPoint)):
        # leur position est tirés aléatoirement entre 0 et 1
        current_x = random()
        current_y = random()
        point = [current_x, current_y]
        # si leur position est dans le cercle alors on le comptabilise dans les points dans le cercle
        if isInCircle(point,1):
            pointInCircle = pointInCircle + 1
            ax.plot(current_x, current_y, 'ro')
        else:
            ax.plot(current_x, current_y, 'bo')
        totalOfPoint = totalOfPoint + 1
    fig.canvas.draw_idle()
    print(4*pointInCircle/totalOfPoint)


def main():
    computePoints(10)
    
    axfreq = plt.axes([0.25, 0.1, 0.65, 0.03])
    freq_slider = Slider(
        ax=axfreq,
        label='Point Number',
        valmin=10,
        valmax=500,
        valinit=10,
    )
    freq_slider.on_changed(computePoints)
    plt.show()


if __name__ == "__main__":
    main()