
# ceci est un caractère seul (char)
var = 'i'
# ceci est une chaine de caractère (string)
var = "toto"
print(var)
# ceci est un entier (int)
var = 12
print(var)
# ceci est un flotant (float)
var = 12.0


var = "toto"
var1 = 12

# impossible car pas le même type
# test = var+var1

# possible car même type
var = 1
test = var+var1

# du type de var car float l'emporte
var = 1.0
test = var+var1
print(test)

# du type de var car float l'emporte
test = var1+var
print(test)