import sys

def addition(a, b):
    return a+b

def multiplication(a,b):
    return a*b

def soustraction(a,b):
    return a-b

def division(a,b):
    return a/b

def main(num1, num2, operation):
    num1 = int(num1)
    num2 = int(num2)
    if(operation == "+"):
        print(addition(num1, num2))
    if(operation == "-"):
        print(soustraction(num1, num2))
    if(operation == "/"):
        print(division(num1, num2))
    if(operation == "*"):
        print(multiplication(num1, num2))
        
main(sys.argv[1], sys.argv[3], sys.argv[2])
    